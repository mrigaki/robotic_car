#colors=[['green', 'green', 'green'],
#        ['green', 'red', 'red'],
#        ['green', 'green', 'green']]
#
#measurements=['red', 'red']
#
#motions=[[0,0], [0,1]]
#
#sensor_right = 1.0
#p_move = 1.0
#
#def show(p):
#    for i in range(len(p)):
#        print p[i]

colors = [['red', 'green', 'green', 'red' , 'red'],
          ['red', 'red', 'green', 'red', 'red'],
          ['red', 'red', 'green', 'green', 'red'],
          ['red', 'red', 'red', 'red', 'red']]

measurements = ['green', 'green', 'green' ,'green', 'green']


motions = [[0,0],[0,1],[1,0],[1,0],[0,1]]

sensor_right = 0.7

p_move = 0.8

def show(p):
    for i in range(len(p)):
        print p[i]

#DO NOT USE IMPORT
#ENTER CODE BELOW HERE
#ANY CODE ABOVE WILL CAUSE
#HOMEWORK TO BE GRADED
#INCORRECT

def move(p,U):
    q = [[0.0 for row in colors[0]] for col in colors]

    for i in range(len(colors)):
        for j in range(len(colors[i])):
            s = p[(i-U[0])%len(p)][(j-U[1])%len(p[i])]*p_move
            q[i][j] = s + p[i][j]*(1-p_move)
    return q

def sense(p,Z):
    s=0
    q = [[0.0 for row in colors[0]] for col in colors]

    for i in range(len(p)):
        for j in range(len(p[i])):
            if (colors[i][j] == Z):
                q[i][j] = p[i][j] * sensor_right
            else:
                q[i][j] = p[i][j] * (1-sensor_right)
            s+=q[i][j]
                   
    for i in range(len(p)):
        for j in range(len(p[0])):    
            q[i][j] = q[i][j]/s  
                   
    return q

num_elements = len(colors)* len(colors[0])

p = [[1.0/num_elements for row in colors[0]] for col in colors]

for k in range(len(measurements)):
    p = move(p,motions[k])
    p = sense(p,measurements[k])

#Your probability array must be printed 
#with the following code.

show(p)